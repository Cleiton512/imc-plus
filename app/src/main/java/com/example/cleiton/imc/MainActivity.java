package com.example.cleiton.imc;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText edtPeso;
    EditText edtAltura;
    TextView txt_imc;
    Button btnCalcular;
    ImageView imv_perfil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtPeso = findViewById(R.id.edtPeso);
        edtAltura = findViewById(R.id.edtAltura);
        txt_imc = findViewById(R.id.txt_imc);
        btnCalcular = findViewById(R.id.btnCalcular);
        imv_perfil = findViewById(R.id.imv_perfil);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                Double peso = Double.parseDouble(edtPeso.getText().toString());
                Double altura = Double.parseDouble(edtAltura.getText().toString()) / 100;
                Double imc = peso / (altura * altura);

                txt_imc.setText(getResources().getText(R.string.imc) + " :" + imc.toString());
                if (imc < 18.5)
                    imv_perfil.setImageDrawable(getDrawable(R.drawable.muitomagro));
                else if (18.6 < imc && imc < 24.9)
                    imv_perfil.setImageDrawable(getDrawable(R.drawable.normal));
                else if (25.0 < imc && imc < 29.9)
                    imv_perfil.setImageDrawable(getDrawable(R.drawable.sobrepeso));
                else if (30.0 < imc && imc < 34.9)
                    imv_perfil.setImageDrawable(getDrawable(R.drawable.obesidade1));
                else if (35.0 < imc && imc < 39.9)
                    imv_perfil.setImageDrawable(getDrawable(R.drawable.obesidade2));
                else if (imc >= 40)
                    imv_perfil.setImageDrawable(getDrawable(R.drawable.obesidade3));

            }
        });
    }
}
